
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class testServlet
 */
@WebServlet("/testServlet")
public class testServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public testServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String str = "https://stonedev.blob.core.windows.net/customer/SystemStone/StoneLab/AutoReport/Ma/ExcelAM/34/2019/06/AM_2019-06-27.zip";

		String str2 = "https://stonedev.blob.core.windows.net/customer/SystemStone/StoneLab/AutoReport/Ma/ExcelAM/34/2019/06/AM_2019-06-27.zip";
		String str3 = "https://stonedev.blob.core.windows.net/customer/SystemStone/StoneLab/AutoReport/Ma/ExcelAM/34/2019/06/AM_2019-06-27.zip";
		String str4 = "https://stonedev.blob.core.windows.net/customer/SystemStone/StoneLab/AutoReport/Ma/ExcelAM/34/2019/06/AM_2019-06-27.zip";
		System.out.println(str.substring(str.lastIndexOf("/")).replaceAll("/", "").trim());
		System.out.println(str2.substring(str2.lastIndexOf("/")-1).replaceAll("/", "").trim());
		System.out.println(str3.substring(str3.lastIndexOf("/")-2).replaceAll("/", "").trim());
		response.getWriter().print(str);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private String getNameFromUri(String uri) {
		return uri.substring(uri.lastIndexOf("/") - 2).replaceAll("/", "").trim();
	}
}
